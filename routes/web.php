<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     $username = 'Toshif';
//     $mobile = '01839430533';
//     // return view('home')->with(['name'=>$username, 'mobile'=>$mobile]); //tansfer data by with method
//     // return view('home',['name'=>$username, 'mobile'=>$mobile]); //tansfer data using array
//     return view('home',compact('username', 'mobile')); //tansfer data using compact method
// });

Route::get('/','HomeController@index');
Route::get('/create','HomeController@create')->name('create');

Route::get('/go', function () {
    return view('welcome');
});
